#!/bin/bash
# Ordem de definição de cores: intro + estilo + cor + m (m determina o fim do trecho)
#vermelho = "31"
#azul = "34"
#branco = "37"
#verde = "32"

# Instalação do INET & ReaSE: Início
echo -e "\033[01;31m\t[INET & ReaSE]: Iniciando Instalação\033[00m"
cd /home/

# Obter INET
echo -e "\033[01;31m\t[INET]: Baixando...\033[00m"
wget https://bitbucket.org/guilhercsoares/preset/src/795757a1074235a8f11d6db37e7395e1e51907cc/inet-20100723-src.tgz

# Extrair
echo -e "\033[01;31m\t[INET]: Extraindo...\033[00m"
tar xfz inet-20100723-src.tgz

# Obter ReaSE (Obter URL)
echo -e "\033[01;31m\t[ReaSE]: Baixando...\033[00m"
wget https://bitbucket.org/guilhercsoares/preset/src/795757a1074235a8f11d6db37e7395e1e51907cc/ReaSE-OMNeT4-1.23-src.tar.gz

# Extrair (Depende do nome do arquivo)
echo -e "\033[01;31m\t[ReaSE]: Extraindo...\033[00m"
tar xfz ReaSE-OMNeT4-1.23-src.tar.gz

# Limpando os arquivos desnecessários (Falta inserir o arquivo do ReaSE)
echo -e "\033[01;31m\t[INET & ReaSE]: Limpando...\033[00m"
rm inet-20100723-src.tgz 
rm ReaSE-OMNeT4-1.23-src.tar.gz

# Instalação do INET & ReaSE: Fim

# Instalação do XMLRPC-C++: Início
echo -e "\033[01;31m\t[XMLRPC-C]: Baixando e Instalando\033[00m"
sudo apt-get install libxmlrpc-core-c3 e sudo apt-get install libxmlrpc-c++4-dev
# Instalação do XMLRPC-C++: Fim

# Instalação do Boost & Xerces: Início
echo -e "\033[01;31m\t[Boost & Xerces]: Baixando e Instalando\033[00m"
sudo apt-get install liblibboost1.46-dev libxerces-c-dev
# Instalação do Boost & Xerces: Fim