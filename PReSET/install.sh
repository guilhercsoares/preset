#!/bin/bash
echo -e "\033[01;31m[PReSET]: Instalando\033[00m"
echo -e "\033[01;31m[PReSET]: Invocando instalador OMNeT++\033[00m"
sleep 5
./install_omnetpp.sh
clear
echo -e "\033[01;31m[PReSET]: Invocando instalador INET & ReaSE\033[00m"
sleep 5
./install_inet_rease.sh
clear
echo -e "\033[01;31m[PReSET]: Invocando instalador Distack & Ponder2\033[00m"
sleep 5
./install_distack_ponder.sh
clear
echo -e "\033[01;31m[PReSET]: Instalado\033[00m"
