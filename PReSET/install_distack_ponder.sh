#!/bin/bash
# Ordem de definição de cores: intro + estilo + cor + m (m determina o fim do trecho)
#vermelho = "31"
#azul = "34"
#branco = "37"
#verde = "32"

# Instalação do Distack: Inicio
echo -e "\033[01;31m\t[Distack]: Iniciando Instalação\033[00m"
sleep 5
cd /home/

# Obter Distack
echo -e "\033[01;31m\t[Distack]: Baixando...\033[00m"
sleep 5
wget https://bitbucket.org/guilhercsoares/preset/src/795757a1074235a8f11d6db37e7395e1e51907cc/distack-1.2.3-dev.tar.gz

# Extrair
echo -e "\033[01;31m\t[Distack]: Extraindo...\033[00m"
sleep 5
tar xfz distack-1.2.3-dev.tar.gz

# Copia do arquivo de patch para a pasta do ReaSE
echo -e "\033[01;31m\t[Distack]: Copiando arquivo de patch...\033[00m"
sleep 5
cd distack-1.2.3-dev/etc/patches/rease

cp distack_rease.patch /home/

cd /home/

# Aplicando patch
echo -e "\033[01;31m\t[Distack]: Aplicando patch..\033[00m"
sleep 5
patch -p0 < ./distack_rease.patch

# Configurando
echo -e "\033[01;31m\t[Distack]: Configurando...\033[00m"
sleep 5
./configure CPPFLAGS='-I/home/omnetpp4/include -I/home/inet/src/networklayer/ipv4 -I/home/inet/src/base -I/home/inet/src/networklayer/contract -I/home/inet/src/networklayer/common -I/home/inet/src/linklayer/contract -I/home/inet/src/transport/tcp -I/home/inet/src/transport/udp -I/home/inet/src/transport/contract -I/home/inet/src/networklayer/arp -I/home/ReaSE/src/transport/contract -I/home/ReaSE/src/base -I/home/ReaSE/src/applications/util' --enable-simulation=yes

# Compilando
echo -e "\033[01;31m\t[Distack]: Compilando...\033[00m"
sleep 5
make

# Limpando os arquivos desnecessários
echo -e "\033[01;31m\t[Distack]: Limpando...\033[00m"
sleep 5
rm distack-1.2.3-dev.tar.gz

# Pacote de políticas/Ponder2: Início
cd /home/

# Instalando o ant
echo -e "\033[01;31m\t[Ponder2]: Instalando ant...\033[00m"
sleep 5
sudo apt-get install ant

# Obtendo os arquivos
echo -e "\033[01;31m\t[Ponder2]: Baixando...\033[00m"
sleep 5
wget https://bitbucket.org/guilhercsoares/preset/src/795757a1074235a8f11d6db37e7395e1e51907cc/simulator_deliverable_1_1.zip
# http://www.comp.lancs.ac.uk/~schaeffe/simulator_deliverable_1_1.zip

#
echo -e "\033[01;31m\t[Ponder2]: Extraindo...\033[00m"
sleep 5
unzip simulator_deliverable_1_1.zip

#
cd simulator_deliverable_1_1

#
echo -e "\033[01;31m\t[Ponder2]: Compilando...\033[00m"
sleep 5
ant rebuild

rm simulator_deliverable_1_1.zip
#