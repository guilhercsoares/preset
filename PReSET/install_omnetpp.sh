#!/bin/bash
# Ordem de definição de cores: intro + estilo + cor + m (m determina o fim do trecho)
#vermelho = "31"
#azul = "34"
#branco = "37"
#verde = "32"

echo -e "\033[01;31m[OMNeT++]: Iniciando Instalação\033[00m"
# Instalação do OMNeT++ 4.1: Início
cd /home/

# Obter OMNeT++ 4.1
echo -e "\033[01;31m\t[OMNeT++]: Baixando...\033[00m"
sleep 5
# URL = http://www.omnetpp.org/omnetpp/doc_download/2217-omnet-41-source--ide-tgz
wget https://bitbucket.org/guilhercsoares/preset/src/795757a1074235a8f11d6db37e7395e1e51907cc/omnetpp-4.1-src.tgz

# Extrair
echo -e "\033[01;31m\t[OMNeT++]: Extraindo...\033[00m"
sleep 5
tar xfz omnet-4.1-src.tgz

# Pacotes extras requiridos
echo -e "\033[01;31m\t[OMNeT++]: Obtendo Pacotes Extras necessarios...\033[00m"
sleep 5
sudo apt-get install g++ bison flex zlib1g-dev tcl8.5-dev tk8.5-dev openjdk-7-jre

# Indo para o diretório
cd omnetpp-4.1

#
. setenv

#
echo -e "\033[01;31m\t[OMNeT++]: Configurando...\033[00m"
sleep 5
./configure

#
echo -e "\033[01;31m\t[OMNeT++]: Compilando...\033[00m"
sleep 5
make

#
echo -e "\033[01;31m\t[OMNeT++]: Criando Itens de Menu e Desktop...\033[00m"
sleep 5
make install-menu-item
make install-desktop-icon

# Limpando os arquivos desnecessários
echo -e "\033[01;31m\t[OMNeT++]: Limpando...\033[00m"
sleep 5
rm omnet-4.1-src.tgz

echo -e "\033[01;31m\t[OMNeT++]: Finalizado\033[00m"
exit